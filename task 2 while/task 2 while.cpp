﻿#include <stdio.h>
#include <Windows.h>
#include <math.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    double s = 0.0;
    int i;
    printf("Введіть натуральне число: ");
    scanf_s("%d", &i);
    while (i) {
        s += 1 / pow(i, 5);
        i--;
    }
    printf("Результат: %f", s);
    return 0;
}