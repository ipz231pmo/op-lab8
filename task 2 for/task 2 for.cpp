﻿#include <stdio.h>
#include <Windows.h>
#include <math.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    double s = 0.0;
    int n;
    printf("Введіть натуральне число: ");
    scanf_s("%d", &n);
    for (int i = 1; i <= n; i++) {
        s += 1 / pow(i, 5);
    }
    printf("Результат: %f", s);
    return 0;
}