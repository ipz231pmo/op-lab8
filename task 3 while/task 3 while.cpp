﻿#include <stdio.h>
#include <Windows.h>
#include <math.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    double a = 3, b = 4, h = 1, i = a, f;

    while (i<=b) {
        f = pow(i, 1. / 3);
        printf("Результат функції F(%3.1f)(корінь кубічний з х) на проміжку [%1.0f, %1.0f] = %f\n", i, a, b, f);
        i+=h;
    }
    a = 0.4;
    b = 1;
    h = 0.1;
    i = a;
    while (i <= b) {
        f = 1 + pow(log(i), 2);
        printf("Результат функції F(%3.1f)(1 + ln^2(x)) на проміжку [%3.1f, %3.1f] = %f\n", i, a, b, f);
        i += h;
    }
    return 0;
}