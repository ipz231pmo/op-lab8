﻿#include <stdio.h>
int main() {
    for (int i = 100; i < 1000; i++) {
        switch (i){
        case 111:
        case 222:
        case 333:
        case 444:
        case 555:
        case 666:
        case 777:
        case 888:
        case 999:
            continue;
        }
        printf("%i\n", i);
    }
    return 0;
}