﻿#include <stdio.h>
#include <Windows.h>
#include <math.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    for (double x = 3; x <= 4; x++) {
        double f = pow(x, 1. / 3);
        printf("Результат функції F(%3.1f)(корінь кубічний з х) на проміжку [3.0, 4.0] = %f\n", x, f);
    }
    for (double x = 0.4; x <= 1; x+=0.1) {
        double f = 1 + pow(log(x), 2);
        printf("Результат функції F(%3.1f)(1 + ln^2(x)) на проміжку [0.4, 1.0] = %f\n", x, f);
    }
    return 0;
}