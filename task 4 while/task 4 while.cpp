﻿#include <stdio.h>
int main() {
    int i = 100;
    while (i < 1000) {
        switch (i) {
        case 111:
        case 222:
        case 333:
        case 444:
        case 555:
        case 666:
        case 777:
        case 888:
        case 999:
            i++;
            continue;
        }
        printf("%i\n", i);
        i++;
    }
    return 0;
}