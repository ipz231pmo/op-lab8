﻿#include <stdio.h>
#include <Windows.h>

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int s = 0;
    int i = 10;
    while (i) {
        s += i * 5;
        i--;
    }
    printf("Сума перших 10 чисел кратних 5 дорівнює %i", s);
    return 0;
}

